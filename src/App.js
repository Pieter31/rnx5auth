import React, { Component } from 'react';
import {firebase} from '@react-native-firebase/auth';
import { createStackNavigator } from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

import Login from './screens/Login';
import Splash from './screens/Splash';
import Main from './screens/Main';
import SignUp from './screens/SignUp';

const Stack = createStackNavigator();

class App extends Component{
  state={loggedIn:null,loading: true};

  componentDidMount(){
    firebase.auth().onAuthStateChanged((user)=>{
      if (user) {
        this.setState({loggedIn: true, loading: false});
      }else{
        this.setState({loggedIn:false, loading: false});
      }
    });
  }
  
  render(){
    if (this.state.loading) {

      return <Splash />;
      
    }
    return(
      <NavigationContainer>
      <Stack.Navigator>
        {this.state.loggedIn ==false ? (
          <>

            <Stack.Screen name ='Login' component={Login}/>
            <Stack.Screen name='SignUp' component={SignUp}/>
            
          </>
        ):(
          <>

          <Stack.Screen name='Main' component={Main} />

          </>
        )}

      </Stack.Navigator>
      </NavigationContainer>
    )
  }
}
  

export default App;