import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, Button } from 'react-native';
import {Spinner,CardSection,Card} from '../components'
import {firebase} from '@react-native-firebase/auth';

class Login extends Component{
  state = { email: '', password: '', errorMessage: null, loading: false }

  handleLogin = () => {
    const { email, password } = this.state;
    
    if (email == ''||password=='') {

      this.setState({errorMessage: "Email or Password is blank"})

    }
    else {
      
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(this.setState({loading: true}))
        .catch(
          error => this.setState({ errorMessage: error.message,loading: false})
        )

    }

  }

  isLoading() {
    this.setState({loading: false});
  }

    

  renderButton() {

    if (this.state.loading) {
      return <Spinner size = "small" />
    }
    else{
      return(

        <CardSection>
          <Button 
           title="Login" 
            onPress={this.handleLogin} 
          />
        </CardSection>

      );
    }
  };

  render(){

    return (
      <Card>

        {this.state.errorMessage &&
          <CardSection>
            <Text style={{ color: 'red' }}>
              {this.state.errorMessage}
            </Text>
          </CardSection>
        }

        <CardSection>
          <TextInput
            style={styles.textInput}
            autoCapitalize="none"
           placeholder="Email"
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />

          <TextInput
            secureTextEntry
            style={styles.textInput}
            autoCapitalize="none"
            placeholder="Password"
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
        </CardSection>
      
        {this.renderButton()}

        <CardSection>
          <Button
            title="Don't have an account? Sign Up"
            onPress={() => this.props.navigation.navigate('SignUp')}
          />
        </CardSection>

      </Card>
    )
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  }
})

export default Login;