import React, { Component } from 'react';
import { Button,StyleSheet ,Text , View} from 'react-native';
import {Spinner,Card,CardSection} from '../components';
import {firebase} from '@react-native-firebase/auth';

class Main extends Component{
  state = { currentUser: null, loading: false, errorMessage: null }

  componentDidMount() {
    const { currentUser } = firebase.auth()
    this.setState({ currentUser })
  }

  handleLogout = () => {

    firebase
      .auth()
      .signOut()
      .then(this.setState({loading:true}))
      .catch(error => this.setState({ errorMessage: error.message,loading:false}))
    ;

  };

  renderButton(){

    if (this.state.loading) {
      
      return <Spinner size="small"/>

    } else {
      
      return(
        <Button 
          title="Sign Out"
          onPress={this.handleLogout.bind(this)}
        />
      );

    }
  }

  render() {
    const { currentUser } = this.state
    return (

      <Card>

        {this.state.errorMessage &&
          <CardSection>
            <Text style={{ color: 'red' }}>
              {this.state.errorMessage}
            </Text>
          </CardSection>
        }

        <Text>
          Hi {currentUser && currentUser.email}!
        </Text>

        {this.renderButton()}

      </Card>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Main;