import React, { Component } from 'react'
import { StyleSheet, Text, TextInput, View, Button } from 'react-native'
import {Spinner,Card,CardSection} from '../components';
import {firebase} from '@react-native-firebase/auth';

class SignUp extends Component{
  state = { email: '', password: '', errorMessage: null,loading: false }

  handleSignUp = () => {

    const { email, password } = this.state

    if (email==''||password=='') {
          
      this.setState({errorMessage: "Email or Password is blank"})

    } else {
      
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(this.setState({loading:true}))
        .catch(error => this.setState({ errorMessage: error.message,loading: false}))
    
    }

  }

  renderButton(){

    if (this.state.loading) {

      return <Spinner size = "small" />
          
    } else {

      return(
          
        <CardSection>
          <Button
            title="Sign Up"
            onPress={this.handleSignUp}
          />
        </CardSection>

      );
          
    }
  }

  render(){

    return(

      <Card>

        {this.state.errorMessage &&
          <CardSection>
            <Text style={{ color: 'red' }}>
              {this.state.errorMessage}
            </Text>
          </CardSection>
        }

        <CardSection>
          <TextInput
            placeholder="Email"
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />

          <TextInput
            secureTextEntry
            placeholder="Password"
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
        </CardSection>

        {this.renderButton()}

        <CardSection>
          <Button
            title="Already have an account? Login"
            onPress={() => this.props.navigation.navigate('Login')}
          />
        </CardSection>

      </Card>
    )
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  }
})

export default SignUp;